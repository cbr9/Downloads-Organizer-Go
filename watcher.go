package main

import (
	"errors"
	"github.com/rjeczalik/notify"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	watcher := newWatcher()
	watcher.watch()
}

type watcher struct {
	home      string
	downloads string
	documents string
	pictures  string
}

func newWatcher() *watcher {
	home := os.Getenv("HOME")
    return &watcher{
		home:      home,
		pictures:  path.Join(home, "Pictures"),
		documents: path.Join(home, "Documents"),
		downloads: path.Join(home, "Downloads"),
	}
}

func (w *watcher) watch() {
	c := make(chan notify.EventInfo, 1)
	err := notify.Watch(w.downloads, c, notify.InMovedTo)
	if err != nil {
		log.Fatal(err)
	}
	defer notify.Stop(c)
	for {
		change := <-c
		switch change.Event() {
		case notify.InMovedTo:
			extension := filepath.Ext(change.Path())
			invalidExts := []string{".part", ".crdownload"}
			isValid := true
			for _, ext := range invalidExts {
				if extension == ext {
					isValid = false
				}
			}
			if isValid {
				file, _ := os.Open(change.Path())
				filetype, _ := getFileContentType(file)
				if extension == ".pdf" {
					w.move(change.Path(), w.documents)
				} else if strings.HasPrefix(filetype, "image") {
                    w.move(change.Path(), w.pictures)
                }
			}
		}
	}
}

func (w *watcher) move(srcPath string, dstPath string) {
	_, err := os.Stat(dstPath)
	if err != nil {
		log.Println(errors.New("target directory " + dstPath + " does not exist. Creating it"))
		err = os.Mkdir(dstPath, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
	}
	newPath := path.Join(dstPath, path.Base(srcPath))
    time.Sleep(5 * time.Second)
	err = os.Rename(srcPath, newPath)
	if err != nil {
		log.Println(err)
	}
}

func getFileContentType(out *os.File) (string, error) {
	buffer := make([]byte, 512)
	_, err := out.Read(buffer)
	if err != nil {
		log.Println(err)
	}
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}
